import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import latest from 'promise-latest'
import { v1 as uuid} from 'uuid'
import _ from 'lodash'

import './index.styl'

import MultiSelect from '../src/components/MultiSelect'

// import dict from './dict'

const dict = () =>({
	response: {
		items: [
			{ id: '1', title: 'Макароны' },
			{ id: '2', title: 'Тушонка' },
			{ id: '3', title: 'Картошка' },
			{ id: '4', title: 'Фарш' },
			{ id: '5', title: 'Куриная грудка' }
		]
	}
});

const loader = (props) => {
	return (
		<img src="loader.gif" {...props}/>
	)
}

class App extends React.Component {
	constructor(...args) {
		super(...args);

		this.fetchOptions = latest(this.fetchOptions.bind(this));
		
		this.state = {
			simpleAsync: {
				value: [{ value: '5', label: 'Куриная грудка' }],
				loading: false
			}
		};
		
		this.cache = {};
	}
	
	handleCreate(text) {
		this.setState({
			...this.state,
			simpleAsync: {
				...this.state.simpleAsync,
				value: [
					...this.state.simpleAsync.value,
					{
						label: text,
						value: uuid()
					}
				]
			}
		})
	}

	handleChange(value) {
		this.setState({
			...this.state,
			simpleAsync: {
				...this.state.simpleAsync,
				value
			}
		});
	}
	
	fetchOptions(text) {
		if(this.cache[text]) return Promise.resolve(this.cache[text]);
		
		this.setState({
			...this.state,
			simpleAsync: {
				...this.state.simpleAsync,
				loading: true
			}
		});
		
		return new Promise((resolve, reject) => {
			setTimeout(() => {
				const filtered = dict().response.items
					.filter((item) => item.title.toLowerCase().indexOf(text.toLowerCase()) + 1)
					.map(item => ({
						value: item.id,
						label: item.title
					}));
				
				this.cache[text] = filtered;
				
				this.setState({
					...this.state,
					simpleAsync: {
						...this.state.simpleAsync,
						loading: false
					}
				});
				
				resolve(filtered);
			}, 200);
		});
	}

	render() {
		const { simpleAsync } = this.state;

		return (
			<div className="example">
				<div className="example__cnt">
					<div className="example__row">
						<div className="example__half">
							<h3>Обычный мультиселект</h3>
							 Обычка
						</div>
						<div className="example__half">
							<h3>Асинхронная работа</h3>
							<MultiSelect value={simpleAsync.value}
										 className="example__multiselect"
										 placeholder={'Выберите страны, в которых вам удавалось побывать'}
										 loading={simpleAsync.loading}
										 loadingComponent={loader}
										 fetchOptions={this.fetchOptions}
										 onChange={::this.handleChange}
							/>
						</div>
						<div className="example__half">
							<h3>Создание нового</h3>
							<MultiSelect value={simpleAsync.value}
										 create={true}
										 onCreate={::this.handleCreate}
										 className="example__multiselect"
										 placeholder={'Выберите страны, в которых вам удавалось побывать'}
										 loading={simpleAsync.loading}
										 loadingComponent={loader}
										 fetchOptions={this.fetchOptions}
										 onChange={::this.handleChange}
							/>
						</div>
					</div>
				</div>
			</div>
		)
	}
}

ReactDOM.render(<App/>, document.querySelector('.page'));
