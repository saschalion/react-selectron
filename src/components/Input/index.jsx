import React, {Component} from 'react'
import AutosizeInput from 'react-input-autosize'
import cx from 'classnames'

class Input extends Component {
	constructor(...args) {
		super(...args);
	}

	handleKeyDown(e) {
		const {onEnter, onBackspace, onMove} = this.props;

		if (e.which == 38 || e.which == 40) {
			e.preventDefault();
			onMove && onMove(e.which === 40 ? 'down' : 'up')
		}

		if (e.which == 13) {
			e.preventDefault();
			onEnter(e.target.value);
		} else if (e.target.value === '') {
			switch (e.which) {
				case 8: {
					e.preventDefault();
					onBackspace && onBackspace();
					break;
				}
			}
		}
	}

	handleInput(e) {
		const { onChange } = this.props;

		onChange && onChange(e.target.value);
	}
	
	getRef() {
		return this._element;
	}
	handleBlur() {
		const {onBlur} = this.props;
		onBlur && onBlur();
	}
	
	handleFocus() {
		const {onFocus} = this.props;
		onFocus && onFocus();
	}

	render() {
		const {className, value, disabled, showPlaceholder, classNames } = this.props;
		
		const placeholder = showPlaceholder ? this.props.placeholder : '';

		return (
			<AutosizeInput ref={(c) => { this._element = c}}
						   onFocus={::this.handleFocus}
						   placeholder={placeholder}
						   onInput={::this.handleInput}
						   onKeyDown={::this.handleKeyDown}
						   onBlur={::this.handleBlur}
						   value={value}
						   inputClassName={cx(className, classNames.input)}
						   className={className}
			/>
		)
	}
}

export default Input
