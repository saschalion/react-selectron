import React, {PureComponent} from 'react'
import cn from 'classnames'

class Option extends PureComponent {
	
	render() {
		const {className, label, focused, onClick, onMouseEnter, classNames} = this.props;

		const rootClasses = cn(classNames.option, className, {
			[classNames['option--focused']]: focused
		});

		return (
			<div onClick={onClick}
				 onMouseEnter={onMouseEnter}
				 className={rootClasses}
			>
				{label}
			</div>
		)
	}
}

export default Option
