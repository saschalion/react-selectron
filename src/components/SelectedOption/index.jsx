import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import cn from 'classnames'

class SelectedOption extends Component {
	handleClick(e) {
		const {onClick} = this.props;
		e.preventDefault();
		e.stopPropagation();

		onClick && onClick();
	}

	handleRemoveClick(e) {
		const {onDelete} = this.props;
		e.stopPropagation();
		
		onDelete && onDelete();
	}

	shouldComponentUpdate(nextProps) {
		return nextProps.focused !== this.props.focused;
	}

	componentWillReceiveProps(nextProps) {
		const {focused} = nextProps;

		if (focused) {
			setTimeout(() => {
				ReactDOM.findDOMNode(this._component).focus();
			}, 1)
		}
	}

	handleKeyDown(e) {
		const {onDelete} = this.props;

		if (e.which == 8) {
			onDelete && onDelete();
		}
	}

	render() {
		const {className, children, focused, classNames} = this.props;

		const rootClasses = cn(className, classNames.selectedOption, {
			[classNames['selectedOption--focused']]: focused
		});

		return (
			<span ref={(c) => this._component = c}
				  onClick={::this.handleClick}
				  tabIndex={-1}
				  onKeyDown={::this.handleKeyDown}
				  className={rootClasses}>
				<span className={classNames.selectedOption__caption}>
					{children}
				</span>
				<span className={classNames.selectedOption__remove}
					  onClick={::this.handleRemoveClick}/>
			</span>
		)
	}
}

export default SelectedOption
