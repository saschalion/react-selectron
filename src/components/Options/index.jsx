import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import cn from 'classnames'
import _ from 'lodash'

import Option from '../Option'

class Options extends Component {
	static defaultProps = {
		options: [],
		loading: false
	}
	
	constructor(...args) {
		super(...args);
		this._options = [];
	}

	componentDidMount() {
		const {wrapper, options} = this.props;
	}
	
	scrollControl(option, direction) {
		const container = ReactDOM.findDOMNode(this._container);
		const element = ReactDOM.findDOMNode(option === 'new' ? this._newOption : this._options[option]);
		
		switch(direction) {
			case "down": {
				if((element.offsetTop + element.offsetHeight) > container.clientHeight + container.scrollTop) {
					container.scrollTop = (element.offsetTop + element.offsetHeight) - container.clientHeight;
				}
				break;
			}
			case "up": {
				if(element.offsetTop < container.scrollTop){
					container.scrollTop = element.offsetTop; 
				}
				break;
			}
			case "end": {
				container.scrollTop = container.scrollHeight;
				break;
			}
			case "start": {
				container.scrollTop = 0;
				break;
			}
		}
	}

	render() {
		const {
			className,
			wrapper,
			onSelect,
			onCreateNew,
			onMouseOver, 
			loading,
			focused,
			options,
			create,
			text,
			classNames,
			...rest
		} = this.props;
		
		const showPlaceholder = !loading && !options.length && !create;
		
		const showOptions = !loading && !!options.length;
		
		return (
			<div className={cn(classNames.options, className)}
				 ref={(c) => this._container = c}
				 style={{width: `${wrapper.offsetWidth}px`}}
				 {...rest}>
				{(create && text) && (
					<Option key={-1}
							ref={(c) => this._newOption = c}
							onMouseEnter={() => onMouseOver('new')}
							onClick={() => onCreateNew(text)}
							classNames={classNames}
							focused={focused === 'new'}
							label={`Cоздать новую «${text}»`}
					/>
				)}
				{ showPlaceholder && <div className={classNames.options__empty}>Ничего не найдено</div>}
				{ showOptions && options.map((option, index) => {
					return (
						<Option key={option.value}
								ref={(c) => this._options[option.value] = c}
								onMouseEnter={() => onMouseOver(option.value)}
								onClick={() => onSelect(option.value)}
								classNames={classNames}
								focused={focused === option.value}
								{...option}
						/>
					)
				})}
			</div>
		)
	}
}

export default Options
