import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import cn from 'classnames'
import _ from 'lodash'
import memoize from 'fast-memoize'

import TetherComponent from 'react-tether'

import Input from '../Input'
import SelectedOption from '../SelectedOption'
import Options from '../Options'

import defaultProps from './defaultProps'
import propTypes from './propTypes'

const findByLabel = (items, label, labelKey = 'label', caseSensitivity = true) => {
	const toLowerCase = caseSensitivity ? (text) => text.toLowerCase() : text => text;
	
	return _.find(items, (option) => {
		return toLowerCase(option[labelKey]) === toLowerCase(label);
	});
}

const getUniqueOptionsByValues = (options, values, valueKey = 'value') => options.filter((option) => {
	return !_.find(values, {[valueKey]: option.value});
});

class MultiSelect extends Component {
	static defaultProps = defaultProps;
	static propTypes = propTypes;
	
	constructor(...args) {
		super(...args);
		
		const { create } = this.props;
		
		this.handleClickOutside = this.handleClickOutside.bind(this);
		this.getOnlyUniqOptions = this.getOnlyUniqOptions.bind(this);
		
		this.getUniqueOptionsByValues = memoize(getUniqueOptionsByValues);
		this.findByLabel = memoize(findByLabel);
		
		this.updateOptions = _.debounce(this.updateOptions, 200);

		this.state = {
			text: '',
			optionsVisibility: false,
			focus: null,
			options: [],
			loading: false,
			focusedOption: create ? 'new' : null,
			inputEditable: false
		};
	}
	
	componentDidMount() {
		document.body.addEventListener('click', this.handleClickOutside);
	}

	componentWillUnmount() {
		document.body.removeEventListener('click', this.handleClickOutside);
	}
	
	handleBackspace() {
		const {value} = this.props;

		if(value.length) {
			const newState = {
				...this.state,
				optionsVisibility: false,
				inputEditable: false,
				focusedOption: null,
				text: '',
				focus: value[value.length - 1]
			};
			
			this.setState(newState, () => {
				this.setInputBlured();
			});
		}
	}

	handleDelete(option) {
		const {onChange, value} = this.props;
		const {focus} = this.state;
		const result = _.without(value, option);

		if (focus) {
			const focusedIndex = value.indexOf(focus) || 1;
			
			this.setState({
				...this.state,
				optionsVisibility: false,
				inputEditable: false,
				focusedOption: null,
				text: '',
				focus: result[focusedIndex - 1]
			});
		}

		onChange && onChange(result);

		!result.length && this.showOptions(); 
	}

	handleClick() {
		const { optionsVisibility} = this.state;

		this.setFocusOnInput();
		if(optionsVisibility) return;
		this.showOptions();
	}

	handleClickOutside(e) {
		const _wrapper = ReactDOM.findDOMNode(this._wrapper);
		const _options = ReactDOM.findDOMNode(this._options);

		const eventOnWrapper = _wrapper && (e.target === _wrapper || _wrapper.contains(e.target));
		const eventOnOptions = _options && (e.target === _options || _options.contains(e.target));

		if (!eventOnWrapper && !eventOnOptions) {
			this.setState({
				...this.state,
				optionsVisibility: false,
				inputEditable: false,
				focus: null,
				focusedOption: null,
				text: '',
				options: []
			});
		}
	}

	handleCreateNew(text) {
		text = text.trim();
		const { onCreate } = this.props;
		this.setState({...this.state, text: ''});

		if (text) {
			onCreate && onCreate(text);
			this.updateOptions('');
		}
	}

	handleInputChange(text) {
		this.setState({
			...this.state,
			text,
			inputEditable: true,
			optionsVisibility: true
		}, () => {
			this.updateOptions(text);
		});
	}

	handleOptionClick(option) {
		this.setState({
			...this.state,
			focus: option,
			optionsVisibility: false,
			inputEditable: false,
			text: '',
			focusedOption: null
		});
	}

	handleMove(type, afterMove = () => {}) {
		let index;
		let { focusedOption } = this.state;
		const options = this.getOnlyUniqOptions(this.state.options);
		
		if(!options.length) return;
		
		switch(type) {
			case 'down': {
				index = this.getFocusIndex(1);
				if(index === 0 || index === 'new') type = 'start';
				break;
			}
			case 'up': {
				index = this.getFocusIndex(-1);
				if(index === options.length - 1) type = 'end';
				break;
			}
		}
		
		focusedOption = (index === 'new') ? 'new' : options[index].value;

		this.setState({...this.state, focusedOption }, () => {
			this._options.scrollControl(focusedOption, type);
			afterMove();
		});
	}

	handleMouseOverOption(focusedOption) {
		this.setState({ ...this.state, focusedOption });
	}

	handleSelect(option) {
		const { value, onChange } = this.props;
		const { loading, options, focusedOption } = this.state;
		
		const target = _.find(this.getOnlyUniqOptions(options), { value: focusedOption })
		
		if(loading || !target) return;
		
		this.handleMove('down', () => {
			onChange && onChange([].concat(value, target));
			
			this.setFocusOnInput();
			this.setState({...this.state, text: ''}, () => {
				this.updateOptions('');
			});	
		});
	}

	handleEnter() {
		const {focusedOption, text} = this.state;
		if(focusedOption === 'new') {
			this.handleCreateNew(text);
		} else {
			this.handleSelect(focusedOption);
		}
	}
	
	createNewAvailable() {
		const { value, create } = this.props;
		const text = this.state.text.trim();
		
		const options = this.getOnlyUniqOptions(this.state.options);
		const textExistsInValue = this.findByLabel(value, text);
		const textExistsInOptions = this.findByLabel(options, text);
		const textIsEmpty = !text.trim().length;
		
		return create && !textExistsInValue && !textExistsInOptions && !textIsEmpty;
	}

	getOnlyUniqOptions() {
		const { value } = this.props;
		const { options } = this.state;

		return this.getUniqueOptionsByValues(options, value);
	}
	
	getFocusedOption() {
		const {focusedOption} = this.state;
		
		const options = this.getOnlyUniqOptions(this.state.options);
		
		if(focusedOption === 'new') return 'new';
		
		return _.find(options, { value: focusedOption });
	}

	getFocusIndex(direction) {
		const {focusedOption} = this.state;

		const options = this.getOnlyUniqOptions(this.state.options);
		let target = _.findIndex(options, this.getFocusedOption(focusedOption)) + direction;

		if(this.createNewAvailable()) {
			if(focusedOption === 'new') {
				if(direction === 1) return 0;
				if(direction === -1) return options.length - 1;
			}

			if((target > options.length - 1) || (target < 0)) return 'new';
		}

		switch (direction) {
			case 1: {
				return target > options.length - 1 ? 0 : target;
			}
			case -1: {
				return target < 0 ? options.length - 1: target;
			}
		}
	}

	setFocusOnInput() {
		this._input._element.focus();
	}

	setInputBlured() {
		this._input._element.blur();
	}

	updateOptions(text = '') {
		const {fetchOptions, create} = this.props;
		const {focusedOption} = this.state;

		this.setState({...this.state, loading: true});

		fetchOptions(text)
			.then((options) => {
				const prevOption = _.find(options, {value: focusedOption});
				// Сохраняем фокус, если в новом списке есть тот же элемент
				let newFocusedOption = (prevOption && prevOption.value) || (options[0] && options[0].value);
				
				if(this.createNewAvailable() && !options.length){
					newFocusedOption = 'new';
				}
				this.setState({...this.state, options, loading: false, focusedOption: newFocusedOption});
			});
	}

	showOptions() {
		this.setState({
			...this.state,
			optionsVisibility: true,
			inputEditable: true,
			focus: null
		}, () => {
			this.setFocusOnInput();
			this.updateOptions();
		});
	}

	render() {
		const {className, value, loading, loadingComponent, create, classNames, placeholder} = this.props;
		const {optionsVisibility, focus, focusedOption, text, inputEditable} = this.state;

		const options = this.getOnlyUniqOptions(this.state.options);
		
		const inputHandlers = {
			// onFocus: ::this.handleInputFocus,
			// onBlur: ::this.hanldeInputBlur,
			onMove: ::this.handleMove,
			onChange: ::this.handleInputChange,
			onEnter: ::this.handleEnter,
			onBackspace: ::this.handleBackspace
		};

		return (
			<TetherComponent attachment="top center">
				<div className={cn(classNames.multiselect, className)}
					 onClick={::this.handleClick}
					 ref={(c) => this._wrapper = c}>
					<div className={classNames.multiselect__selectedOptions}
						 ref={(c) => this._selectedOptions = c}>
						{ value.map((option) => (
							<SelectedOption key={option.value}
											focused={focus === option}
											onClick={() => this.handleOptionClick(option)}
											onDelete={() => this.handleDelete(option)}
											classNames={classNames}
											className={classNames.multiselect__option}>
								{option.label}
							</SelectedOption>
						))}
						<Input className={classNames.multiselect__input}
							   value={text}
							   disabled={inputEditable}
							   placeholder={placeholder}
							   showPlaceholder={!value.length}
							   {...inputHandlers}
							   classNames={classNames}
							   ref={(c) => this._input = c}
						/>
					</div>
					{loading && React.createElement(loadingComponent, { className: classNames.multiselect__loader })}
				</div>
				{optionsVisibility && (
					<Options className={classNames.multiselect__options}
							 ref={(c) => this._options = c}
							 focused={focusedOption}
							 loading={loading}
							 create={this.createNewAvailable()}
							 text={text}
							 value={value}
							 options={options}
							 onMouseOver={::this.handleMouseOverOption}
							 onSelect={::this.handleSelect}
							 onCreateNew={::this.handleCreateNew}
							 classNames={classNames}
							 wrapper={this._wrapper}
					/>
				)}
			</TetherComponent>
		)
	}
}

export default MultiSelect
