'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var createRef = exports.createRef = function createRef(name) {
  return function (c) {
    return undefined['_' + name] = c;
  };
};