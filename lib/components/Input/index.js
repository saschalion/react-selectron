'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactInputAutosize = require('react-input-autosize');

var _reactInputAutosize2 = _interopRequireDefault(_reactInputAutosize);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Input = function (_Component) {
	_inherits(Input, _Component);

	function Input() {
		var _ref;

		_classCallCheck(this, Input);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		return _possibleConstructorReturn(this, (_ref = Input.__proto__ || Object.getPrototypeOf(Input)).call.apply(_ref, [this].concat(args)));
	}

	_createClass(Input, [{
		key: 'handleKeyDown',
		value: function handleKeyDown(e) {
			var _props = this.props,
			    onEnter = _props.onEnter,
			    onBackspace = _props.onBackspace,
			    onMove = _props.onMove;


			if (e.which == 38 || e.which == 40) {
				e.preventDefault();
				onMove && onMove(e.which === 40 ? 'down' : 'up');
			}

			if (e.which == 13) {
				e.preventDefault();
				onEnter(e.target.value);
			} else if (e.target.value === '') {
				switch (e.which) {
					case 8:
						{
							e.preventDefault();
							onBackspace && onBackspace();
							break;
						}
				}
			}
		}
	}, {
		key: 'handleInput',
		value: function handleInput(e) {
			var onChange = this.props.onChange;


			onChange && onChange(e.target.value);
		}
	}, {
		key: 'getRef',
		value: function getRef() {
			return this._element;
		}
	}, {
		key: 'handleBlur',
		value: function handleBlur() {
			var onBlur = this.props.onBlur;

			onBlur && onBlur();
		}
	}, {
		key: 'handleFocus',
		value: function handleFocus() {
			var onFocus = this.props.onFocus;

			onFocus && onFocus();
		}
	}, {
		key: 'render',
		value: function render() {
			var _this2 = this;

			var _props2 = this.props,
			    className = _props2.className,
			    value = _props2.value,
			    disabled = _props2.disabled,
			    showPlaceholder = _props2.showPlaceholder,
			    classNames = _props2.classNames;


			var placeholder = showPlaceholder ? this.props.placeholder : '';

			return _react2.default.createElement(_reactInputAutosize2.default, { ref: function ref(c) {
					_this2._element = c;
				},
				onFocus: this.handleFocus.bind(this),
				placeholder: placeholder,
				onInput: this.handleInput.bind(this),
				onKeyDown: this.handleKeyDown.bind(this),
				onBlur: this.handleBlur.bind(this),
				value: value,
				inputClassName: (0, _classnames2.default)(className, classNames.input),
				className: className
			});
		}
	}]);

	return Input;
}(_react.Component);

exports.default = Input;