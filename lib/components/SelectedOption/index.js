'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SelectedOption = function (_Component) {
	_inherits(SelectedOption, _Component);

	function SelectedOption() {
		_classCallCheck(this, SelectedOption);

		return _possibleConstructorReturn(this, (SelectedOption.__proto__ || Object.getPrototypeOf(SelectedOption)).apply(this, arguments));
	}

	_createClass(SelectedOption, [{
		key: 'handleClick',
		value: function handleClick(e) {
			var onClick = this.props.onClick;

			e.preventDefault();
			e.stopPropagation();

			onClick && onClick();
		}
	}, {
		key: 'handleRemoveClick',
		value: function handleRemoveClick(e) {
			var onDelete = this.props.onDelete;

			e.stopPropagation();

			onDelete && onDelete();
		}
	}, {
		key: 'shouldComponentUpdate',
		value: function shouldComponentUpdate(nextProps) {
			return nextProps.focused !== this.props.focused;
		}
	}, {
		key: 'componentWillReceiveProps',
		value: function componentWillReceiveProps(nextProps) {
			var _this2 = this;

			var focused = nextProps.focused;


			if (focused) {
				setTimeout(function () {
					_reactDom2.default.findDOMNode(_this2._component).focus();
				}, 1);
			}
		}
	}, {
		key: 'handleKeyDown',
		value: function handleKeyDown(e) {
			var onDelete = this.props.onDelete;


			if (e.which == 8) {
				onDelete && onDelete();
			}
		}
	}, {
		key: 'render',
		value: function render() {
			var _this3 = this;

			var _props = this.props,
			    className = _props.className,
			    children = _props.children,
			    focused = _props.focused,
			    classNames = _props.classNames;


			var rootClasses = (0, _classnames2.default)(className, classNames.selectedOption, _defineProperty({}, classNames['selectedOption--focused'], focused));

			return _react2.default.createElement(
				'span',
				{ ref: function ref(c) {
						return _this3._component = c;
					},
					onClick: this.handleClick.bind(this),
					tabIndex: -1,
					onKeyDown: this.handleKeyDown.bind(this),
					className: rootClasses },
				_react2.default.createElement(
					'span',
					{ className: classNames.selectedOption__caption },
					children
				),
				_react2.default.createElement('span', { className: classNames.selectedOption__remove,
					onClick: this.handleRemoveClick.bind(this) })
			);
		}
	}]);

	return SelectedOption;
}(_react.Component);

exports.default = SelectedOption;