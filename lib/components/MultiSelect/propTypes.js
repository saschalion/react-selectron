'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

exports.default = {
	caseSensitivity: _react.PropTypes.bool,
	onChange: _react.PropTypes.func,
	value: _react.PropTypes.array,
	valueKey: _react.PropTypes.string,
	labelKey: _react.PropTypes.string
};