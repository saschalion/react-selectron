'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _classNames = require('../../classNames.js');

var _classNames2 = _interopRequireDefault(_classNames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
	caseSensitivity: true,
	value: [],
	classNames: _classNames2.default,
	valueKey: 'value',
	labelKey: 'label'
};