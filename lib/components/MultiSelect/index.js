'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _findIndex2 = require('lodash/findIndex');

var _findIndex3 = _interopRequireDefault(_findIndex2);

var _without2 = require('lodash/without');

var _without3 = _interopRequireDefault(_without2);

var _debounce2 = require('lodash/debounce');

var _debounce3 = _interopRequireDefault(_debounce2);

var _find3 = require('lodash/find');

var _find4 = _interopRequireDefault(_find3);

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _fastMemoize = require('fast-memoize');

var _fastMemoize2 = _interopRequireDefault(_fastMemoize);

var _reactTether = require('react-tether');

var _reactTether2 = _interopRequireDefault(_reactTether);

var _Input = require('../Input');

var _Input2 = _interopRequireDefault(_Input);

var _SelectedOption = require('../SelectedOption');

var _SelectedOption2 = _interopRequireDefault(_SelectedOption);

var _Options = require('../Options');

var _Options2 = _interopRequireDefault(_Options);

var _defaultProps = require('./defaultProps');

var _defaultProps2 = _interopRequireDefault(_defaultProps);

var _propTypes = require('./propTypes');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var findByLabel = function findByLabel(items, label) {
	var labelKey = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'label';
	var caseSensitivity = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : true;

	var toLowerCase = caseSensitivity ? function (text) {
		return text.toLowerCase();
	} : function (text) {
		return text;
	};

	return (0, _find4.default)(items, function (option) {
		return toLowerCase(option[labelKey]) === toLowerCase(label);
	});
};

var getUniqueOptionsByValues = function getUniqueOptionsByValues(options, values) {
	var valueKey = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'value';
	return options.filter(function (option) {
		return !(0, _find4.default)(values, _defineProperty({}, valueKey, option.value));
	});
};

var MultiSelect = function (_Component) {
	_inherits(MultiSelect, _Component);

	function MultiSelect() {
		var _ref;

		_classCallCheck(this, MultiSelect);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		var _this = _possibleConstructorReturn(this, (_ref = MultiSelect.__proto__ || Object.getPrototypeOf(MultiSelect)).call.apply(_ref, [this].concat(args)));

		var create = _this.props.create;


		_this.handleClickOutside = _this.handleClickOutside.bind(_this);
		_this.getOnlyUniqOptions = _this.getOnlyUniqOptions.bind(_this);

		_this.getUniqueOptionsByValues = (0, _fastMemoize2.default)(getUniqueOptionsByValues);
		_this.findByLabel = (0, _fastMemoize2.default)(findByLabel);

		_this.updateOptions = (0, _debounce3.default)(_this.updateOptions, 200);

		_this.state = {
			text: '',
			optionsVisibility: false,
			focus: null,
			options: [],
			loading: false,
			focusedOption: create ? 'new' : null,
			inputEditable: false
		};
		return _this;
	}

	_createClass(MultiSelect, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			document.body.addEventListener('click', this.handleClickOutside);
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			document.body.removeEventListener('click', this.handleClickOutside);
		}
	}, {
		key: 'handleBackspace',
		value: function handleBackspace() {
			var _this2 = this;

			var value = this.props.value;


			if (value.length) {
				var newState = _extends({}, this.state, {
					optionsVisibility: false,
					inputEditable: false,
					focusedOption: null,
					text: '',
					focus: value[value.length - 1]
				});

				this.setState(newState, function () {
					_this2.setInputBlured();
				});
			}
		}
	}, {
		key: 'handleDelete',
		value: function handleDelete(option) {
			var _props = this.props,
			    onChange = _props.onChange,
			    value = _props.value;
			var focus = this.state.focus;

			var result = (0, _without3.default)(value, option);

			if (focus) {
				var focusedIndex = value.indexOf(focus) || 1;

				this.setState(_extends({}, this.state, {
					optionsVisibility: false,
					inputEditable: false,
					focusedOption: null,
					text: '',
					focus: result[focusedIndex - 1]
				}));
			}

			onChange && onChange(result);

			!result.length && this.showOptions();
		}
	}, {
		key: 'handleClick',
		value: function handleClick() {
			var optionsVisibility = this.state.optionsVisibility;


			this.setFocusOnInput();
			if (optionsVisibility) return;
			this.showOptions();
		}
	}, {
		key: 'handleClickOutside',
		value: function handleClickOutside(e) {
			var _wrapper = _reactDom2.default.findDOMNode(this._wrapper);
			var _options = _reactDom2.default.findDOMNode(this._options);

			var eventOnWrapper = _wrapper && (e.target === _wrapper || _wrapper.contains(e.target));
			var eventOnOptions = _options && (e.target === _options || _options.contains(e.target));

			if (!eventOnWrapper && !eventOnOptions) {
				this.setState(_extends({}, this.state, {
					optionsVisibility: false,
					inputEditable: false,
					focus: null,
					focusedOption: null,
					text: '',
					options: []
				}));
			}
		}
	}, {
		key: 'handleCreateNew',
		value: function handleCreateNew(text) {
			text = text.trim();
			var onCreate = this.props.onCreate;

			this.setState(_extends({}, this.state, { text: '' }));

			if (text) {
				onCreate && onCreate(text);
				this.updateOptions('');
			}
		}
	}, {
		key: 'handleInputChange',
		value: function handleInputChange(text) {
			var _this3 = this;

			this.setState(_extends({}, this.state, {
				text: text,
				inputEditable: true,
				optionsVisibility: true
			}), function () {
				_this3.updateOptions(text);
			});
		}
	}, {
		key: 'handleOptionClick',
		value: function handleOptionClick(option) {
			this.setState(_extends({}, this.state, {
				focus: option,
				optionsVisibility: false,
				inputEditable: false,
				text: '',
				focusedOption: null
			}));
		}
	}, {
		key: 'handleMove',
		value: function handleMove(type) {
			var _this4 = this;

			var afterMove = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : function () {};

			var index = void 0;
			var focusedOption = this.state.focusedOption;

			var options = this.getOnlyUniqOptions(this.state.options);

			if (!options.length) return;

			switch (type) {
				case 'down':
					{
						index = this.getFocusIndex(1);
						if (index === 0 || index === 'new') type = 'start';
						break;
					}
				case 'up':
					{
						index = this.getFocusIndex(-1);
						if (index === options.length - 1) type = 'end';
						break;
					}
			}

			focusedOption = index === 'new' ? 'new' : options[index].value;

			this.setState(_extends({}, this.state, { focusedOption: focusedOption }), function () {
				_this4._options.scrollControl(focusedOption, type);
				afterMove();
			});
		}
	}, {
		key: 'handleMouseOverOption',
		value: function handleMouseOverOption(focusedOption) {
			this.setState(_extends({}, this.state, { focusedOption: focusedOption }));
		}
	}, {
		key: 'handleSelect',
		value: function handleSelect(option) {
			var _this5 = this;

			var _props2 = this.props,
			    value = _props2.value,
			    onChange = _props2.onChange;
			var _state = this.state,
			    loading = _state.loading,
			    options = _state.options,
			    focusedOption = _state.focusedOption;


			var target = (0, _find4.default)(this.getOnlyUniqOptions(options), { value: focusedOption });

			if (loading || !target) return;

			this.handleMove('down', function () {
				onChange && onChange([].concat(value, target));

				_this5.setFocusOnInput();
				_this5.setState(_extends({}, _this5.state, { text: '' }), function () {
					_this5.updateOptions('');
				});
			});
		}
	}, {
		key: 'handleEnter',
		value: function handleEnter() {
			var _state2 = this.state,
			    focusedOption = _state2.focusedOption,
			    text = _state2.text;

			if (focusedOption === 'new') {
				this.handleCreateNew(text);
			} else {
				this.handleSelect(focusedOption);
			}
		}
	}, {
		key: 'createNewAvailable',
		value: function createNewAvailable() {
			var _props3 = this.props,
			    value = _props3.value,
			    create = _props3.create;

			var text = this.state.text.trim();

			var options = this.getOnlyUniqOptions(this.state.options);
			var textExistsInValue = this.findByLabel(value, text);
			var textExistsInOptions = this.findByLabel(options, text);
			var textIsEmpty = !text.trim().length;

			return create && !textExistsInValue && !textExistsInOptions && !textIsEmpty;
		}
	}, {
		key: 'getOnlyUniqOptions',
		value: function getOnlyUniqOptions() {
			var value = this.props.value;
			var options = this.state.options;


			return this.getUniqueOptionsByValues(options, value);
		}
	}, {
		key: 'getFocusedOption',
		value: function getFocusedOption() {
			var focusedOption = this.state.focusedOption;


			var options = this.getOnlyUniqOptions(this.state.options);

			if (focusedOption === 'new') return 'new';

			return (0, _find4.default)(options, { value: focusedOption });
		}
	}, {
		key: 'getFocusIndex',
		value: function getFocusIndex(direction) {
			var focusedOption = this.state.focusedOption;


			var options = this.getOnlyUniqOptions(this.state.options);
			var target = (0, _findIndex3.default)(options, this.getFocusedOption(focusedOption)) + direction;

			if (this.createNewAvailable()) {
				if (focusedOption === 'new') {
					if (direction === 1) return 0;
					if (direction === -1) return options.length - 1;
				}

				if (target > options.length - 1 || target < 0) return 'new';
			}

			switch (direction) {
				case 1:
					{
						return target > options.length - 1 ? 0 : target;
					}
				case -1:
					{
						return target < 0 ? options.length - 1 : target;
					}
			}
		}
	}, {
		key: 'setFocusOnInput',
		value: function setFocusOnInput() {
			this._input._element.focus();
		}
	}, {
		key: 'setInputBlured',
		value: function setInputBlured() {
			this._input._element.blur();
		}
	}, {
		key: 'updateOptions',
		value: function updateOptions() {
			var _this6 = this;

			var text = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
			var _props4 = this.props,
			    fetchOptions = _props4.fetchOptions,
			    create = _props4.create;
			var focusedOption = this.state.focusedOption;


			this.setState(_extends({}, this.state, { loading: true }));

			fetchOptions(text).then(function (options) {
				var prevOption = (0, _find4.default)(options, { value: focusedOption });
				// Сохраняем фокус, если в новом списке есть тот же элемент
				var newFocusedOption = prevOption && prevOption.value || options[0] && options[0].value;

				if (_this6.createNewAvailable() && !options.length) {
					newFocusedOption = 'new';
				}
				_this6.setState(_extends({}, _this6.state, { options: options, loading: false, focusedOption: newFocusedOption }));
			});
		}
	}, {
		key: 'showOptions',
		value: function showOptions() {
			var _this7 = this;

			this.setState(_extends({}, this.state, {
				optionsVisibility: true,
				inputEditable: true,
				focus: null
			}), function () {
				_this7.setFocusOnInput();
				_this7.updateOptions();
			});
		}
	}, {
		key: 'render',
		value: function render() {
			var _this8 = this;

			var _props5 = this.props,
			    className = _props5.className,
			    value = _props5.value,
			    loading = _props5.loading,
			    loadingComponent = _props5.loadingComponent,
			    create = _props5.create,
			    classNames = _props5.classNames,
			    placeholder = _props5.placeholder;
			var _state3 = this.state,
			    optionsVisibility = _state3.optionsVisibility,
			    focus = _state3.focus,
			    focusedOption = _state3.focusedOption,
			    text = _state3.text,
			    inputEditable = _state3.inputEditable;


			var options = this.getOnlyUniqOptions(this.state.options);

			var inputHandlers = {
				// onFocus: ::this.handleInputFocus,
				// onBlur: ::this.hanldeInputBlur,
				onMove: this.handleMove.bind(this),
				onChange: this.handleInputChange.bind(this),
				onEnter: this.handleEnter.bind(this),
				onBackspace: this.handleBackspace.bind(this)
			};

			return _react2.default.createElement(
				_reactTether2.default,
				{ attachment: 'top center' },
				_react2.default.createElement(
					'div',
					{ className: (0, _classnames2.default)(classNames.multiselect, className),
						onClick: this.handleClick.bind(this),
						ref: function ref(c) {
							return _this8._wrapper = c;
						} },
					_react2.default.createElement(
						'div',
						{ className: classNames.multiselect__selectedOptions,
							ref: function ref(c) {
								return _this8._selectedOptions = c;
							} },
						value.map(function (option) {
							return _react2.default.createElement(
								_SelectedOption2.default,
								{ key: option.value,
									focused: focus === option,
									onClick: function onClick() {
										return _this8.handleOptionClick(option);
									},
									onDelete: function onDelete() {
										return _this8.handleDelete(option);
									},
									classNames: classNames,
									className: classNames.multiselect__option },
								option.label
							);
						}),
						_react2.default.createElement(_Input2.default, _extends({ className: classNames.multiselect__input,
							value: text,
							disabled: inputEditable,
							placeholder: placeholder,
							showPlaceholder: !value.length
						}, inputHandlers, {
							classNames: classNames,
							ref: function ref(c) {
								return _this8._input = c;
							}
						}))
					),
					loading && _react2.default.createElement(loadingComponent, { className: classNames.multiselect__loader })
				),
				optionsVisibility && _react2.default.createElement(_Options2.default, { className: classNames.multiselect__options,
					ref: function ref(c) {
						return _this8._options = c;
					},
					focused: focusedOption,
					loading: loading,
					create: this.createNewAvailable(),
					text: text,
					value: value,
					options: options,
					onMouseOver: this.handleMouseOverOption.bind(this),
					onSelect: this.handleSelect.bind(this),
					onCreateNew: this.handleCreateNew.bind(this),
					classNames: classNames,
					wrapper: this._wrapper
				})
			);
		}
	}]);

	return MultiSelect;
}(_react.Component);

MultiSelect.defaultProps = _defaultProps2.default;
MultiSelect.propTypes = _propTypes2.default;
exports.default = MultiSelect;