'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _Option = require('../Option');

var _Option2 = _interopRequireDefault(_Option);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Options = function (_Component) {
	_inherits(Options, _Component);

	function Options() {
		var _ref;

		_classCallCheck(this, Options);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		var _this = _possibleConstructorReturn(this, (_ref = Options.__proto__ || Object.getPrototypeOf(Options)).call.apply(_ref, [this].concat(args)));

		_this._options = [];
		return _this;
	}

	_createClass(Options, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			var _props = this.props,
			    wrapper = _props.wrapper,
			    options = _props.options;
		}
	}, {
		key: 'scrollControl',
		value: function scrollControl(option, direction) {
			var container = _reactDom2.default.findDOMNode(this._container);
			var element = _reactDom2.default.findDOMNode(option === 'new' ? this._newOption : this._options[option]);

			switch (direction) {
				case "down":
					{
						if (element.offsetTop + element.offsetHeight > container.clientHeight + container.scrollTop) {
							container.scrollTop = element.offsetTop + element.offsetHeight - container.clientHeight;
						}
						break;
					}
				case "up":
					{
						if (element.offsetTop < container.scrollTop) {
							container.scrollTop = element.offsetTop;
						}
						break;
					}
				case "end":
					{
						container.scrollTop = container.scrollHeight;
						break;
					}
				case "start":
					{
						container.scrollTop = 0;
						break;
					}
			}
		}
	}, {
		key: 'render',
		value: function render() {
			var _this2 = this;

			var _props2 = this.props,
			    className = _props2.className,
			    wrapper = _props2.wrapper,
			    onSelect = _props2.onSelect,
			    onCreateNew = _props2.onCreateNew,
			    onMouseOver = _props2.onMouseOver,
			    loading = _props2.loading,
			    focused = _props2.focused,
			    options = _props2.options,
			    create = _props2.create,
			    text = _props2.text,
			    classNames = _props2.classNames,
			    rest = _objectWithoutProperties(_props2, ['className', 'wrapper', 'onSelect', 'onCreateNew', 'onMouseOver', 'loading', 'focused', 'options', 'create', 'text', 'classNames']);

			var showPlaceholder = !loading && !options.length && !create;

			var showOptions = !loading && !!options.length;

			return _react2.default.createElement(
				'div',
				_extends({ className: (0, _classnames2.default)(classNames.options, className),
					ref: function ref(c) {
						return _this2._container = c;
					},
					style: { width: wrapper.offsetWidth + 'px' }
				}, rest),
				create && text && _react2.default.createElement(_Option2.default, { key: -1,
					ref: function ref(c) {
						return _this2._newOption = c;
					},
					onMouseEnter: function onMouseEnter() {
						return onMouseOver('new');
					},
					onClick: function onClick() {
						return onCreateNew(text);
					},
					classNames: classNames,
					focused: focused === 'new',
					label: 'C\u043E\u0437\u0434\u0430\u0442\u044C \u043D\u043E\u0432\u0443\u044E \xAB' + text + '\xBB'
				}),
				showPlaceholder && _react2.default.createElement(
					'div',
					{ className: classNames.options__empty },
					'\u041D\u0438\u0447\u0435\u0433\u043E \u043D\u0435 \u043D\u0430\u0439\u0434\u0435\u043D\u043E'
				),
				showOptions && options.map(function (option, index) {
					return _react2.default.createElement(_Option2.default, _extends({ key: option.value,
						ref: function ref(c) {
							return _this2._options[option.value] = c;
						},
						onMouseEnter: function onMouseEnter() {
							return onMouseOver(option.value);
						},
						onClick: function onClick() {
							return onSelect(option.value);
						},
						classNames: classNames,
						focused: focused === option.value
					}, option));
				})
			);
		}
	}]);

	return Options;
}(_react.Component);

Options.defaultProps = {
	options: [],
	loading: false
};
exports.default = Options;