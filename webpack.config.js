const NODE_ENV = process.env.NODE_ENV || 'development';
const webpack = require('webpack');
const path = require('path');

const PATHS = {
	entry: path.resolve(__dirname, 'example/index'),
	build: path.resolve(__dirname, 'example')
};

module.exports = {
	entry: {
		'bundle': PATHS.entry
	},
	output: {
		path: PATHS.build,
		filename: '[name].js',
		chunkFilename: '[id].chunk.js',
	},
	module: {
		loaders: [
			{ test: /\.styl$/, loader: 'style!css!stylus' },
			{
				test: /\.jsx?$/,
				exclude: /node_modules/, 
				loader: "babel"
			}
		]
	},
	plugins: [
		new webpack.optimize.DedupePlugin(),
		new webpack.optimize.OccurenceOrderPlugin(),
		new webpack.DefinePlugin({
			'process.env.NODE_ENV': JSON.stringify(NODE_ENV)
		})
	],
	resolve: {
		modulesDirectories: ["node_modules"],
		extensions: ['', '.js', '.jsx']
	},
	devServer: {
		contentBase: './example',
	},
	devtool: 'source-map',
	watch: true,
	cache: true
}
