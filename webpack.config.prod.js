const NODE_ENV = process.env.NODE_ENV || 'development';
const webpack = require('webpack');
const path = require('path');

const PATHS = {
	src: path.resolve(__dirname, 'src/index'),
	dist: path.resolve(__dirname, 'dist')
};

module.exports = {
	entry: {
		'index': PATHS.src
	},
	output: {
		path: PATHS.lib,
		filename: '[name].js',
		chunkFilename: '[id].chunk.js',
	},
	module: {
		loaders: [
			{ test: /\.jsx?$/, exclude: /node_modules/, loader: "babel" }
		]
	},
	externals: {
		'react': 'React',
		'react-dom': 'ReactDOM'
	},
	plugins: [
		new webpack.optimize.DedupePlugin(),
		new webpack.optimize.OccurenceOrderPlugin(),
		new webpack.DefinePlugin({
			'process.env.NODE_ENV': JSON.stringify(NODE_ENV)
		})
	],
	resolve: {
		modulesDirectories: ["node_modules"],
		extensions: ['', '.js', '.jsx']
	},
	watch: false,
	devtool: false
}
